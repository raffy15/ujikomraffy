<!DOCTYPE html>
<?PHP
include "koneksib.php"
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>INVENTARIS SMKN1CIOMAS</title>

	
	
    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/but.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <marquee><a class="navbar-brand" href="#">&nbsp &nbsp <font color="black" size="8" face="dom casual">Inventaris</marquee></font>&nbsp <font color="black" face="dom casual"></font></a> 
            </div>
            <!-- /.navbar-header -->


           <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-&nbspcollapse">
                    <ul class="nav" id="side-menu">
                        <br>
						<li>
                            <a href="#"><i class="fa fa-dashboard fa-fw" p style="color:black"></i><font color="black"> Dashboard</font></a>
                        </li>
						 <li>
                            <br><p style="color:black"><i class="fa fa-plus fa-fw"></i><font color="black"><b>&nbsp   Input</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tambah_inventaris.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="input_pinjam.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Peminjaman</font></a>
                        </li>
						
						
						<li>
                            <a href="input_jenis.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="input_ruang.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Ruang</font></a>
                        </li>
						
						<li>
                            <br><p style="color:black"><i class="fa fa-folder fa-fw"></i><font color="black"><b>&nbsp   Backup Database</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						
						<li>
                            <a href="#"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Backup</font></a>
                        </li>
						
                        <li>
                            <br><p style="color:black"><i class="fa fa-folder fa-fw"></i><font color="black"><b>&nbsp   Data</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tampil.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="tampil_pinjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Peminjaman</font></a>
                        </li>
						<li>
                            <a href="tampil_meminjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Detail Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_jenis.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tampil_ruang.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Ruang</font></a>
                        </li>
						
						
						<li>
                            <br><a href="../pages/dark/index.php"><i class="fa fa-sign-out" p style="color:black"></i><font color="black"> Logout</font></a>
                        </li>
						
                        
                    </ul>
					
                </div>
				
                <!-- /.sidebar-collapse -->
            </div>
			
            <!-- /.navbar-static-side -->
        </nav>
 
 
 
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
               <?php
error_reporting(0);
$file=date("Ymd").'_backup_database_'.time().'.sql';
backup_tables("localhost","root","","ujikom",$file);
?>   <br>
					<div class="col-md-4">
							<div class="btn btn-primary border-top-primary text-center">
								<h6 class="no-margin text-semibold">Backup Database</h6>
								<p class="text-muted content-group-sm"></p>

		                    	<a type="submit" onclick="location.href='download_backup_data?nama_file=<?php echo $file;?>'" class="btn btn-primary"><b><i class="icon-download"></i></b> Backup</a>
							</div>
					</div>
<?php  
function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*')  {
$link = mysql_connect($host,$user,$pass);
mysql_select_db($name,$link);
if($tables == '*'){
$tables = array();
$result = mysql_query('SHOW TABLES');
while($row = mysql_fetch_row($result)){
$tables[] = $row[0];
}
}
else{//jika hanya table-table tertentu
$tables = is_array($tables) ? $tables : explode(',',$tables);
}
foreach($tables as $table){
$result = mysql_query('SELECT * FROM '.$table);
$num_fields = mysql_num_fields($result);
$return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
$return.= "\n\n".$row2[1].";\n\n";
for ($i = 0; $i < $num_fields; $i++) {
while($row = mysql_fetch_row($result)){
//menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
$return.= 'INSERT INTO '.$table.' VALUES(';
for($j=0; $j<$num_fields; $j++) {
//akan menelusuri setiap baris query didalam
$row[$j] = addslashes($row[$j]);
$row[$j] = ereg_replace("\n","\\n",$row[$j]);
if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
if ($j<($num_fields-1)) { $return.= ','; }
}
$return.= ");\n";
}
}
$return.="\n\n\n";
}                           
//simpan file di folder
$nama_file;
$handle = fopen('backup/'.$nama_file,'w+');
fwrite($handle,$return);
fclose($handle);
}
?>     
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
           
                <!-- /.col-lg-8 -->
             
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
