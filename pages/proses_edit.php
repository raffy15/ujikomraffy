<?php
// Load file koneksi.php
include "koneksi.php";

// Ambil data NIS yang dikirim oleh form_ubah.php melalui URL
$id = $_GET['id'];

// Ambil Data yang Dikirim dari Form
$nama = $_POST['nama'];
$alamat = $_POST['alamat'];
$ttl = $_POST['alamat'];
$email = $_POST['email'];
$no_telpon = $_POST['no_telpon'];

// Cek apakah user ingin mengubah imagenya atau tidak
if(isset($_POST['ubah_image'])){ // Jika user menceklis checkbox yang ada di form ubah, lakukan :
	// Ambil data image yang dipilih dari form
	$image = $_FILES['image']['name'];
	$tmp = $_FILES['image']['tmp_name'];
	
	// Rename nama imagenya dengan menambahkan tanggal dan jam upload
	$imagebaru = date('dmYHis').$image;
	
	// Set path folder tempat menyimpan imagenya
	$path = "gambar/".$imagebaru;

	// Proses upload
	if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
		// Query untuk menampilkan data siswa berdasarkan NIS yang dikirim
		$query = "SELECT * FROM formulir WHERE id='".$id."'";
		$sql = mysql_query($query); // Eksekusi/Jalankan query dari variabel $query
		$data = mysql_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

		// Cek apakah file image sebelumnya ada di folder images
		if(is_file("images/".$data['image'])) // Jika image ada
			unlink("images/".$data['image']); // Hapus file image sebelumnya yang ada di folder images
		
		// Proses ubah data ke Database
		$query = "UPDATE pendaftaran SET nama='".$nama."', alamat='".$alamat."', ttl='".$ttl."', email='".$email."', no_telpon='".$no_telpon."',  image='".$imagebaru."' WHERE id='".$id."'";
		$sql = mysqli_query($konek, $query); // Eksekusi/ Jalankan query dari variabel $query

		if($sql){ // Cek jika proses simpan ke database sukses atau tidak
			// Jika Sukses, Lakukan :
			header("location: tampil.php"); // Redirect ke halaman index.php
		}else{
			// Jika Gagal, Lakukan :
			echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan.";
			echo "<br><a href='edit.php'>Kembali Ke Form</a>";
		}
	}else{
		// Jika gambar gagal diupload, Lakukan :
		echo "Maaf, Gambar gagal untuk diupload.";
		echo "<br><a href='edit.php'>Kembali Ke Form</a>";
	}
}else{ // Jika user tidak menceklis checkbox yang ada di form ubah, lakukan :
	// Proses ubah data ke Database
	$query = "UPDATE formulir SET nama='".$nama."', alamat='".$alamat."',ttl='".$ttl."',email='".$email."', no_telpon='".$no_telpon."' WHERE id='".$id."'";
	$sql = mysql_query( $query); // Eksekusi/ Jalankan query dari variabel $query

	if($sql){ // Cek jika proses simpan ke database sukses atau tidak
		// Jika Sukses, Lakukan :
		header("location: tampil.php"); // Redirect ke halaman index.php
	}else{
		// Jika Gagal, Lakukan :
		echo "Maaf.";
		echo "<br><a href='edit.php'>Kembali Ke Form</a>";
	}
}
?>
