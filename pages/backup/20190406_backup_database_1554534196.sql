DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(100) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_inventaris_2` (`id_inventaris`),
  KEY `id_peminjaman` (`id_peminjaman`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_2` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("76","116","5","dikembalikan","1");
INSERT INTO detail_pinjam VALUES("77","116","0","dipinjam","79");
INSERT INTO detail_pinjam VALUES("78","116","5","dikembalikan","80");
INSERT INTO detail_pinjam VALUES("79","116","1","dikembalikan","81");
INSERT INTO detail_pinjam VALUES("80","116","2","dikembalikan","82");
INSERT INTO detail_pinjam VALUES("81","116","10","dikembalikan","83");
INSERT INTO detail_pinjam VALUES("82","117","15","dikembalikan","84");
INSERT INTO detail_pinjam VALUES("83","116","1","dikembalikan","85");
INSERT INTO detail_pinjam VALUES("84","118","10","dikembalikan","86");
INSERT INTO detail_pinjam VALUES("85","117","1","dikembalikan","88");
INSERT INTO detail_pinjam VALUES("86","116","0","dipinjam","92");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(11) NOT NULL,
  `kondisi` varchar(70) NOT NULL,
  `ket` varchar(30) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(20) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` int(25) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_jenis_2` (`id_jenis`),
  KEY `id_ruang_2` (`id_ruang`),
  KEY `id_petugas_2` (`id_petugas`),
  KEY `id_jenis_3` (`id_jenis`),
  KEY `id_jenis_4` (`id_jenis`),
  KEY `id_ruang_3` (`id_ruang`),
  KEY `id_jenis_5` (`id_jenis`),
  KEY `id_ruang_4` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_4` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("116","kursi","baik","tru","10","5","2019-04-01","2","12","9");
INSERT INTO inventaris VALUES("117","meja","baik","baik","35","5","2019-04-03","2","13","9");
INSERT INTO inventaris VALUES("118","pulpen","baik","baik","10","5","2019-04-04","2","12","9");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("5","Laptop","12","tidak rusak");
INSERT INTO jenis VALUES("6","Meja","6","ndosnfio");
INSERT INTO jenis VALUES("7","cttc","tctc","tctct");
INSERT INTO jenis VALUES("8","nknkn","knknkn","knknknk");
INSERT INTO jenis VALUES("9","aaa","aaa","a");
INSERT INTO jenis VALUES("10","aaa","aaa","a");
INSERT INTO jenis VALUES("11","bb","bbb","bbb");
INSERT INTO jenis VALUES("12","bb","bbb","bbb");
INSERT INTO jenis VALUES("13","bb","bbb","bbb");
INSERT INTO jenis VALUES("14","bb","bbb","bbb");
INSERT INTO jenis VALUES("15","qq","gcgcgcgc","ghcghcghc");
INSERT INTO jenis VALUES("16","as","tftf","tftft");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("10","nnjb","1212","uhuhu");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `peminjaman_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","2019-01-02","2019-01-17","10");
INSERT INTO peminjaman VALUES("53","2019-04-03","2019-04-10","10");
INSERT INTO peminjaman VALUES("54","2019-04-03","2019-04-03","10");
INSERT INTO peminjaman VALUES("55","2019-04-03","2019-04-18","10");
INSERT INTO peminjaman VALUES("56","2019-04-03","2019-04-02","10");
INSERT INTO peminjaman VALUES("57","2019-04-03","2019-04-04","10");
INSERT INTO peminjaman VALUES("58","2019-04-03","2019-04-12","10");
INSERT INTO peminjaman VALUES("59","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("60","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("61","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("62","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("63","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("64","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("65","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("66","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("67","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("68","2019-04-03","2019-04-03","10");
INSERT INTO peminjaman VALUES("69","2019-04-03","2019-05-03","10");
INSERT INTO peminjaman VALUES("70","2019-04-03","2019-04-16","10");
INSERT INTO peminjaman VALUES("71","2019-04-03","2019-04-18","10");
INSERT INTO peminjaman VALUES("72","2019-04-03","2019-04-04","10");
INSERT INTO peminjaman VALUES("73","2019-04-03","2019-04-10","10");
INSERT INTO peminjaman VALUES("74","2019-04-03","2019-04-12","10");
INSERT INTO peminjaman VALUES("75","2019-04-03","2019-04-18","10");
INSERT INTO peminjaman VALUES("76","2019-04-03","2019-04-10","10");
INSERT INTO peminjaman VALUES("77","2019-04-03","2019-04-11","10");
INSERT INTO peminjaman VALUES("78","2019-04-03","2019-04-18","10");
INSERT INTO peminjaman VALUES("79","2019-04-03","2019-04-10","10");
INSERT INTO peminjaman VALUES("80","2019-04-03","2019-04-11","10");
INSERT INTO peminjaman VALUES("81","2019-04-03","2019-04-12","10");
INSERT INTO peminjaman VALUES("82","2019-04-03","2019-04-04","10");
INSERT INTO peminjaman VALUES("83","2019-04-03","2019-04-12","10");
INSERT INTO peminjaman VALUES("84","2019-04-03","2019-04-05","10");
INSERT INTO peminjaman VALUES("85","2019-04-03","2019-04-20","10");
INSERT INTO peminjaman VALUES("86","2019-04-04","2019-04-05","10");
INSERT INTO peminjaman VALUES("87","2019-04-04","2019-04-15","10");
INSERT INTO peminjaman VALUES("88","2019-04-04","2019-04-04","10");
INSERT INTO peminjaman VALUES("89","2019-04-05","2019-04-11","10");
INSERT INTO peminjaman VALUES("90","2019-04-05","2019-04-10","10");
INSERT INTO peminjaman VALUES("91","2019-04-05","2019-04-18","10");
INSERT INTO peminjaman VALUES("92","2019-04-06","2019-04-11","10");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  KEY `id_level_2` (`id_level`),
  CONSTRAINT `petugas_ibfk_2` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("9","admin1","admin123","didi","1");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` enum('Lab 1','Lab 2','R1') NOT NULL,
  `kode_ruang` varchar(30) NOT NULL,
  `ket` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("2","Lab 2","12","baik");
INSERT INTO ruang VALUES("3","R1","12","asam");



