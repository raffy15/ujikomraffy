<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.datatables.css">
    <title>INVENTARIS SMKN 1 CIOMAS</title>

    <!-- Bootstrap Core CSS -->
      <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/datat.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
	<link type="text/css" href="css/jq.css" rel=".">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

     <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <marquee><a class="navbar-brand">&nbsp &nbsp <font color="black" size="8" face="dom casual">Inventaris</marquee></font>&nbsp <font color="black" face="dom casual"></a> 
            </div>
            <!-- /.navbar-header -->


             <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-&nbspcollapse">
                    <ul class="nav" id="side-menu">
                         <br>
						<li>
                            <a href="index1.php"><i class="fa fa-dashboard fa-fw" p style="color:black"></i><font color="black"> Dashboard</font></a>
                        </li>
                        
						 <li>
                            <br><p style="color:black"><i class="fa fa-plus fa-fw"></i><font color="black"><b>&nbsp   Input</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tambah_inventaris.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="input_pinjam.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Peminjaman</font></a>
                        </li>
						
						
						<li>
                            <a href="input_jenis.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tambah_ruang.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Ruang</font></a>
                        </li>
						
						<li>
                            <br><p style="color:black"><i class="fa fa-folder"></i><font color="black"><b>&nbsp   Data</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tampil.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="tampil_pinjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Peminjaman</font></a>
                        </li>
						<li>
                            <a href="tampil_meminjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Detail Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_jenis.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="#"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Ruang</font></a>
                        </li>
						
						<li>
                            <a href="../pages/dark/index.php"><i class="fa fa-sign-out" p style="color:black"></i><font color="black"> Logout</font></a>
                        </li>
						
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
             <div class="row">
                <div class="col-lg-12">

			 <h1 class="page-header">Data Ruang</h1><br>
				&nbsp &nbsp <button class="btn btn-primary"><a href="tambah_ruang.php"><font color="ffffff">tambah data</font></a></button><br><br> 
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="tester" class="table table-striped table-bordered table-hover">
                                  <thead>
                                        <tr class="bg-table">
											<td>No</td>
                                            <td>Nama Ruang</td>
                                            <td>Kode Ruang</td>
                                            <td>Keterangan</td>																						
											<td>Opsi</td>
										</tr>
                                    </thead>
									<tbody>
									<?php
									include "koneksib.php";
									$query_mysqli = mysqli_query ($konek, "SELECT * FROM ruang 
									ORDER BY id_ruang DESC") or die (mysqli_error());
									$i = 1;
									while($data = mysqli_fetch_array($query_mysqli)){
								?>
								<tr>
									<td class="text-center"><?php echo $i++;?></td>
									<td class="text-center"><?php echo $data['nama_ruang']; ?></td>
									<td class="text-center"><?php echo $data['kode_ruang']; ?></td>
									<td class="text-center"><?php echo $data['ket']; ?></td>
									
									<td align="center" width="200px">
										<a class="fa fa-edit" href="edit_ruang.php?id=<?php echo $data['id_ruang'];?>"></a>
										|
										<a class="fa fa-trash-o" href="hapus_ruang.php?id=<?php echo $data['id_ruang'];?>" onclick="return confirm('Apakah anda yakin ingin mengahapus ini?');"></a>
									</td>
								<?php
									}
								?>
								</tr>
									 </tbody>
                              
                                </table></br>
								
	      </div>
    </div>
  </div>
		 
		 
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.datatables.min.js"></script>
	<script>
	$(document).ready (function(){
		$('#tester').DataTable();
	});
	
	</script>
			
    
</div>
</div>
                            </div>
                            </div>

   

</body>

</html>
