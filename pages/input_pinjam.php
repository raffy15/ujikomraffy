<?php
      @session_start();
      include "koneksib.php";       
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>INVENTARIS SMKN1CIOMAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />

		<link rel="stylesheet"
			  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
		<link rel="stylesheet"
			  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>

		<link rel="stylesheet" href="./css/bootstrap-material-datetimepicker.css" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
		<script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
		<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="./js/bootstrap-material-datetimepicker.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <marquee><a class="navbar-brand">&nbsp &nbsp <font color="black" size="8" face="dom casual">Inventaris</marquee></font>&nbsp <font color="black" face="dom casual"></font></a> 
            </div>
            <!-- /.navbar-header -->


           <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-&nbspcollapse">
                    <ul class="nav" id="side-menu">
                         <br>
						<li>
                            <a href="index1.php"><i class="fa fa-dashboard fa-fw" p style="color:black"></i><font color="black"> Dashboard</font></a>
                        </li>
                        
						 
                            <br><li class="header"></li> <p style="color:black"><i class="fa fa-plus fa-fw"></i><font color="black"><b>&nbsp   Input</b></font>
                            <!-- /.nav-second-level -->
                        <br>
						<li>
                            <a href="tambah_inventaris.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="#"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Peminjaman</font></a>
                        </li>
						
						
						<li>
                            <a href="input_jenis.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tambah_ruang.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Ruang</font></a>
                        </li>
						
						<li>
                            <br><p style="color:black"><i class="fa fa-folder"></i><font color="black"><b>&nbsp   Data</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tampil.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="tampil_pinjam.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Peminjaman</font></a>
                        </li>
						<li>
                            <a href="tampil_meminjam.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Detail Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_jenis.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tampil_ruang.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Ruang</font></a>
                        </li>
						
						<li>
                            <a href="../pages/dark/index.php"><i class="fa fa-sign-out" p style="color:black"></i><font color="black"> Logout</font></a>
                        </li>
						
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <BR>
            <!-- /.row -->
           
            <!-- /.row -->
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Masukan Data Pinjam
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form method="POST" action="proses_pinjam.php" class="form">
										
										<div class="form-group">
                                            <label>Tanggal Pinjam</label>
											<?php
											$tanggal_pinjam=date('Y-m-d');											
											?>
                                            <input name ="tanggal_pinjam" value="<?php echo $tanggal_pinjam ?>" class="form-control" id="focusedinput" type="text" readonly>
                                        	
										</div>
										<div class="form-group">
                                            <label>Tanggal Kembali</label>
                                            <input name ="tanggal_kembali" type="text"  class="form-control" id="min-date" required>
                                        </div>
										<div class="form-group">
                                            <label>ID Pengguna</label>											
                                            <select name="id_pegawai" class="form-control" required readonly>
								<?php
								include"koneksib.php";
								$select = mysqli_query($konek, "SELECT * FROM pegawai");
								while($show = mysqli_fetch_array($select)){
									?>
									<option value="<?=$show['id_pegawai'];?>"><?=$show['id_pegawai'];?></option>
								<?php } ?>
								</select>
                                        
										</div>							
										
										<div class="form-actions">
                                        <button type="submit" name="tambah" class="btn btn-default">Tambah</button>
                                        <button type="reset" class="btn btn-default">Batal</button>
                                    </form>
                                </div>
								</fieldset>
								</form>
                                <!-- /.col-lg-6 (nested) -->
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        </div>
		
        <!-- /#page-wrapper -->

    <!-- /#wrapper -->

    <!-- jQuery -->

   
		<script type="text/javascript">
		$(document).ready(function()
		{
			$('#date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});

			$('#time').bootstrapMaterialDatePicker
			({
				date: false,
				shortTime: false,
				format: 'HH:mm'
			});

			$('#date-format').bootstrapMaterialDatePicker
			({
				format: 'dddd DD MMMM YYYY - '
			});
			$('#date-fr').bootstrapMaterialDatePicker
			({
				format: 'DD/MM/YYYY',
				lang: 'fr',
				weekStart: 1, 
				cancelText : 'ANNULER',
				nowButton : true,
				switchOnClick : true
			});

			$('#date-end').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'DD/MM/YYYY'
			});
			$('#date-start').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'DD/MM/YYYY', shortTime : true
			}).on('change', function(e, date)
			{
				$('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
			});

			$('#min-date').bootstrapMaterialDatePicker({ format : 'YY/MM/DD', time: false, minDate : new Date() });

			$.material.init()
		});
		</script>

</body>

</html>
