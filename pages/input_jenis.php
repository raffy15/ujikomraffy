<?php
      @session_start();
      include "koneksib.php";       
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>INVENTARIS SMKN1CIOMAS</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <marquee><a class="navbar-brand" href="#">&nbsp &nbsp <font color="black" size="8" face="dom casual">Inventaris</marquee></font>&nbsp <font color="black" face="dom casual"></font></a> 
            </div>
            <!-- /.navbar-header -->


             <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-&nbspcollapse">
                    <ul class="nav" id="side-menu">
                         <br>
						<li>
                            <a href="index1.php"><i class="fa fa-dashboard fa-fw" p style="color:black"></i><font color="black"> Dashboard</font></a>
                        </li>
                        
						 <li>
                            <br><p style="color:black"><i class="fa fa-plus fa-fw"></i><font color="black"><b>&nbsp   Input</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tambah_inventaris.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="input_pinjam.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Peminjaman</font></a>
                        </li>
						
						
						<li>
                            <a href="#"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tambah_ruang.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Ruang</font></a>
                        </li>
						
						<li>
                            <br><p style="color:black"><i class="fa fa-folder"></i><font color="black"><b>&nbsp   Data</b></font>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="tampil.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Inventaris</font></a>
                        </li>
						<li>
                            <a href="tampil_pinjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Peminjaman</font></a>
                        </li>
						<li>
                            <a href="tampil_meminjam.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Detail Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_jenis.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Jenis</font></a>
                        </li>
						
						<li>
                            <a href="tampil_ruang.php"><i class="fa fa-folder-open fa-fw" p style="color:black"></i><font color="black"> Data Ruang</font></a>
                        </li>
						
						<li>
                            <a href="../pages/dark/index.php"><i class="fa fa-sign-out" p style="color:black"></i><font color="black"> Logout</font></a>
                        </li>
						
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Jenis</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Masukan Data Jenis
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form method="POST" action="proses_tambah_jenis.php" class="form">
                                        <input type="hidden" name ="id_jenis" value="<?php echo $data['id_jenis']?>">
										<div class="form-group">
                                            <label>Jenis</label>
                                            <input name ="nama_jenis"  class="form-control" id="focusedinput" type="text" required>
                                        </div>
										<div class="form-group">
                                            <label>Kode Jenis</label>
                                            <input name ="kode_jenis"  class="form-control" id="focusedinput" type="text" required>
                                        </div>
										<div class="form-group">
                                            <label>Keterangan</label>
                                            <input name ="keterangan"  class="form-control" id="focusedinput" type="text" required>
                                        </div>
										
										<div class="form-actions">
                                        <button type="submit" name="tambah" class="btn btn-default">Tambah</button>
                                        <button type="reset" class="btn btn-default">Batal</button>
                                    </form>
                                </div>
								</fieldset>
								</form>
                                <!-- /.col-lg-6 (nested) -->
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
</form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form></form>