<!DOCTYPE html>
<?PHP
include "koneksib.php"
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>INVENTARIS SMKN1CIOMAS</title>

	
	
    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/but.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <marquee><a class="navbar-brand" href="#">&nbsp &nbsp <font color="black" size="8" face="dom casual">Inventaris</marquee></font>&nbsp <font color="black" face="dom casual"></font></a> 
            </div>
            <!-- /.navbar-header -->


           <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-&nbspcollapse">
                    <ul class="nav" id="side-menu">
                        <br>
                        <br>
						<li>
                            <a href="index2.php"><i class="fa fa-dashboard fa-fw" p style="color:black"></i><font color="black"> Dashboard</font></a>
                        </li>
                        
						 
						
						<li>
                            <a href="input_op_pinjam.php"><i class="fa fa-plus fa-fw" p style="color:black"></i><font color="black"> Input Data Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_pinjam_op.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Peminjaman</font></a>
                        </li>
						
						<li>
                            <a href="tampil_inven_op.php"><i class="fa fa-folder fa-fw" p style="color:black"></i><font color="black"> Data Inventaris</font></a>
                        </li>
						
						<li>
                            <a href="../pages/dark/index.php"><i class="fa fa-sign-out" p style="color:black"></i><font color="black"> Logout</font></a>
                        </li>
						
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Hai Operator !!</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <!-- /.row -->
           
                <!-- /.col-lg-8 -->
              <div class="row">
               <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <br><i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-center"><br>
                                    <div>Jumlah</div>
									<h3>
									<?php
									$query=mysqli_query($konek,"SELECT COUNT(id_inventaris) FROM inventaris");
									$ambilquery= mysqli_fetch_array($query);
									echo $ambilquery[0];
									?>
                                </div>
                            </div>
                        </div>
                      
                            <div class="panel-footer">
                                <span class="pull-left" p style="color:white">Jumlah Data Inventaris</span>                              
                                <div class="clearfix"></div>
                            </div>
							</div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <br><i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 user-center"><br>
                                    <div>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Jumlah</div>
									<h3>
									&nbsp &nbsp &nbsp &nbsp <?php
									$query=mysqli_query($konek,"SELECT COUNT(id_peminjaman) FROM peminjaman");
									$ambilquery= mysqli_fetch_array($query);
									echo $ambilquery[0];
									?>
                                </div>
                            </div>
                        </div>
                        
                            <div class="panel-footer">
                                <span class="pull-left" p style="color:white">Jumlah Data Peminjaman</span>                               
                                <div class="clearfix"></div>
                            </div>
                        
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <br><i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-center"><br>                                    
                                    <div>Jumlah</div>
									<h3>
									<?php
									$query=mysqli_query($konek,"SELECT COUNT(id_detail_pinjam) FROM detail_pinjam");
									$ambilquery= mysqli_fetch_array($query);
									echo $ambilquery[0];
									?>
                                </div>
                            </div>
                        </div>
                        
                            <div class="panel-footer">
                                <span class="pull-left" p style="color:white">Jumlah Data Detail Pinjam</span>
                                
                                <div class="clearfix"></div>
                            </div>
                     
                    </div>
                </div>
                
				
				 
				
            </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
	
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
